module bitbucket.org/kuzmind/kominsearch

require github.com/elastic/go-elasticsearch/v7 v7.5.1-0.20200630125649-09b44b16bdb7

require github.com/gin-gonic/gin v1.6.3

go 1.14
