package main

import (
    "bitbucket.org/kuzmind/kominsearch"
)

func main() {
    r := kominsearch.SetupRouter()
    r.Run(":8080") // Listen and Server in 0.0.0.0:8080
}
