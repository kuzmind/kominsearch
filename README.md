1. Install [GO](https://golang.org/doc/install)
2. Install any IDE which supports GO. For example [VS Code](https://code.visualstudio.com/docs/setup/setup-overview)
3. Run ./app/main.go
